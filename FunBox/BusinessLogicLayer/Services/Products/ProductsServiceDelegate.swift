//
//  ProductsServiceDelegate.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 10.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

protocol ProductsServiceDelegate {
    func productUpdated(product: Product, at index: Int)
    func productAdded(product: Product)
}
