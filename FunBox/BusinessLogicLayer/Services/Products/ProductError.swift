//
//  ProductError.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 09.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

enum ProductError: Error {
    case insufficientQuantityProduct
    case productNotFound
    case testErrorProduct
}

extension ProductError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .insufficientQuantityProduct:
            return NSLocalizedString("Description of insufficient quantity of product", comment: "insufficientQuantityProduct error description")
        case .productNotFound:
            return NSLocalizedString("Description of product not found error", comment: "productNotFound error description")
        case .testErrorProduct:
            return NSLocalizedString("Description of product test error", comment: "testErrorProduct error description")
        }
    }
}
