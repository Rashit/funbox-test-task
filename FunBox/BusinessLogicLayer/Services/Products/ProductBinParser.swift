//
//  ProductBinParser.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 24.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

class ProductBinParser: ProductParserProtocol {
    func productsFromData(_ data: Data) -> [Product] {
        let intSize = 8
        let doubleSize = 8
        var products: [Product] = []
        var recordStartIndex = 0
        while recordStartIndex < data.count {
            let recordSizeDataEndIndex = recordStartIndex + intSize
            let recordSizeData = data[recordStartIndex..<recordSizeDataEndIndex]
            let recordSize = Int.init(data: recordSizeData) ?? 0
            let quantityDataEndIndex   = recordSizeDataEndIndex + intSize
            let priceDataEndIndex      = quantityDataEndIndex + doubleSize
            let nameDataEndIndex       = recordStartIndex + recordSize

            let quantityData = data[recordSizeDataEndIndex..<quantityDataEndIndex]
            let priceData    = data[quantityDataEndIndex..<priceDataEndIndex]
            let nameData     = data[priceDataEndIndex..<nameDataEndIndex]
            if let quantity = Int(data: quantityData),
                let price  = Double(data: priceData),
                let name = String(data: nameData) {
                let product = Product(name: name, quantity: quantity, price: price)
                products.append(product)
            }
            recordStartIndex = nameDataEndIndex
        }
        return products
    }

    func dataFromProducts(_ products: [Product]) -> Data {
        var fileData = Data()
        products.forEach{fileData += $0.record()}
        return fileData
    }
}

private extension Product {
    func record() -> Data {
        let productData = self.quantity.data + self.price.data + self.name.data
        var recordSize: Int = 0
        recordSize = productData.count + MemoryLayout.size(ofValue: recordSize)
        let recordData = recordSize.data + productData
        return recordData
    }
}
