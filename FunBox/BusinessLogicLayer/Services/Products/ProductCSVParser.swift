//
//  ProductCSVParser.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 21.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

class ProductCSVParser: ProductParserProtocol {
    func productsFromData(_ data: Data) -> [Product] {
        var products: [Product] = []
        if let fileContentString = String(data: data, encoding: .utf8) {
            let rows = fileContentString.split{$0.isNewline}
            for row in rows {
                let fields = row.split {$0 == ","}
                if fields.count < 3 {
                    continue
                }
                let name = String(fields[0]).replacingOccurrences(of: "\"", with: "").trimmingCharacters(in: .whitespaces)
                let priceString = String(fields[1]).replacingOccurrences(of: "\"", with: "").trimmingCharacters(in: .whitespaces)
                let price = Double(priceString) ?? Double.zero
                let quantityString = String(fields[2]).replacingOccurrences(of: "\"", with: "").trimmingCharacters(in: .whitespaces)
                let quantity = Int(quantityString) ?? 0
                let product = Product(name: name, quantity: quantity, price: price)
                products.append(product)
            }
        }
        return products
    }

    func dataFromProducts(_ products: [Product]) -> Data {
        var arrayData: Data = Data()
        products.forEach{ arrayData += $0.record() }
        return arrayData
    }
}

private extension Product {
    func record() -> Data {
        ("\"\(self.name)\",\"\(self.price)\",\"\(self.quantity)\"\n").data
    }
}
