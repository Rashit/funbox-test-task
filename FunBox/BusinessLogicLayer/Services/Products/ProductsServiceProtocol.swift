//
//  ProductsServiceProtocol.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 14.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

protocol ProductsServiceProtocol {
    typealias GetProductsSuccessBlock = ([Product]) -> ()
    typealias ServiceSuccessBlock = () -> ()
    typealias ServiceFailBlock = (Error) -> ()

    func initialLoadProducts(success: @escaping ServiceSuccessBlock, fail: @escaping ServiceFailBlock)
    func addSubscriber(_ subscriber: ProductsServiceDelegate)
    func getProducts(success: @escaping GetProductsSuccessBlock, fail: @escaping ServiceFailBlock)
    func saveProduct(_ product: Product, at index: Int, success: @escaping ServiceSuccessBlock, fail: @escaping ServiceFailBlock)
    func addProduct(_ product: Product, success: @escaping ServiceSuccessBlock, fail: @escaping ServiceFailBlock)
    func buyProduct(at index: Int, success: @escaping ServiceSuccessBlock, fail: @escaping ServiceFailBlock)
}
