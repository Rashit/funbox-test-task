//
//  ProductParserProtocol.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 21.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//
import Foundation

protocol ProductParserProtocol {
    func productsFromData(_ data: Data) -> [Product]
    func dataFromProducts(_ products: [Product]) -> Data
}
