//
//  ProductsService.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 14.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

class ProductsService {
    static let shared = ProductsService()
    struct Config {
        var dataManager: ProductsDataManagerProtocol
        var parser: ProductParserProtocol
    }
    private static var config: Config?

    private let dataManager: ProductsDataManagerProtocol
    private let parser: ProductParserProtocol
    private let allProductsSerialQueue: DispatchQueue = DispatchQueue(label: "allProductsSerialQueue")
    private let oneProductConcurrentQueue: OperationQueue
    private var products: [Model] = []
    private var productsIsLoaded: Bool = false
    private var subscribers: [ProductsServiceDelegate?] = []

    class func setup(_ config: Config){
        ProductsService.config = config
    }

    private init() {
        guard let config = ProductsService.config else {
            fatalError("Error - you must call setup before accessing ProductsFileService.shared")
        }

        self.dataManager = config.dataManager
        self.parser = config.parser
        self.oneProductConcurrentQueue = OperationQueue()
        self.oneProductConcurrentQueue.name = "oneProductConcurrentQueue"
    }
}

extension ProductsService: ProductsServiceProtocol {
    func initialLoadProducts(success: @escaping ServiceSuccessBlock, fail: @escaping ServiceFailBlock) {
        self.allProductsSerialQueue.async { [weak self] in
            guard let strongSelf = self else {return}
            strongSelf.productsIsLoaded = false
            //for demo generate error sometime
            let number = Int.random(in: 1...2)
            if number == 1 {
                sleep(3)
                let errorTemp = NSError(domain:"", code:404, userInfo:nil)
                DispatchQueue.main.async {
                    fail(errorTemp)
                }
                return;
            }

            sleep(Constants.productsLoadDurationSeconds)
            do {
                let data = try strongSelf.dataManager.readProducts()
                
                strongSelf.products = strongSelf.parser.productsFromData(data).map {Model(product: $0, lastOp: nil)}
                strongSelf.productsIsLoaded = true
                DispatchQueue.main.async {
                    success()
                }
            }
            catch {
                DispatchQueue.main.async {
                    fail(error)
                }
            }
        }
    }

    func getProducts(success: @escaping GetProductsSuccessBlock, fail: @escaping ServiceFailBlock) {
        self.allProductsSerialQueue.async { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.productsIsLoaded {
                DispatchQueue.main.async {
                    success(strongSelf.products.map{$0.product})
                }
            } else {
                strongSelf.initialLoadProducts(success: {
                    DispatchQueue.main.async {
                        success(strongSelf.products.map{$0.product})
                    }
                }) { (error) in
                    DispatchQueue.main.async {
                        fail(error)
                    }
                }
            }
        }
    }

    func addSubscriber(_ subscriber: ProductsServiceDelegate) {
        self.subscribers.append(subscriber)
    }

    func addProduct(_ product: Product, success: @escaping ServiceSuccessBlock, fail: @escaping ServiceFailBlock) {
        self.allProductsSerialQueue.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.products.append(Model(product: product))
            strongSelf.saveData(success: success, fail: fail)
            strongSelf.notifyAdded(product: product)
        }
    }

    func saveProduct(_ product: Product, at index: Int, success: @escaping ServiceSuccessBlock, fail: @escaping ServiceFailBlock) {

        self.allProductsSerialQueue.async { [weak self] in
            guard let strongSelf = self else {return}
            guard strongSelf.products.indices.contains(index) else {
                let error: ProductError = ProductError.productNotFound
                DispatchQueue.main.async {
                    fail(error)
                }
                return
            }

            let saveOperation = BlockOperation {
                print("start save product")
                sleep(Constants.productSaveDurationSeconds)
                strongSelf.allProductsSerialQueue.async {
                    print("saving product")
                    strongSelf.products[index].product = product
                    strongSelf.saveData(success: success, fail: fail)
                    strongSelf.notifyUpdated(product: product, at: index)
                }
            }

            if let lastOperation = strongSelf.products[index].lastOp {
                saveOperation.addDependency(lastOperation)
            }
            strongSelf.products[index].lastOp = saveOperation
            strongSelf.oneProductConcurrentQueue.addOperation(saveOperation)
        }
    }

    func buyProduct(at index: Int, success: @escaping ServiceSuccessBlock, fail: @escaping ServiceFailBlock) {
        self.allProductsSerialQueue.async { [weak self] in
            guard let strongSelf = self else {return}
            guard strongSelf.products.indices.contains(index) else {
                let error: ProductError = ProductError.productNotFound
                DispatchQueue.main.async {
                    fail(error)
                }
                return
            }

            //for demo generate error sometime
            let number = Int.random(in: 1...2)
            if number == 1 {
                let error: ProductError = ProductError.testErrorProduct
                DispatchQueue.main.async {
                    fail(error)
                }
                return
            }

            let buyOperation = BlockOperation {
                print("start buy product")
                sleep(Constants.productBuyDurationSeconds)
                strongSelf.allProductsSerialQueue.async {
                    print("buying product")
                    if strongSelf.products[index].product.quantity < 1 {
                        let error: ProductError = ProductError.insufficientQuantityProduct
                        DispatchQueue.main.async {
                            fail(error)
                        }
                    } else {
                        strongSelf.products[index].product.quantity -= 1
                        strongSelf.saveData(success: success, fail: fail)
                        let product = strongSelf.products[index].product
                        strongSelf.notifyUpdated(product: product, at: index)
                    }
                }
            }

            if let lastOperation = strongSelf.products[index].lastOp {
                buyOperation.addDependency(lastOperation)
            }
            strongSelf.products[index].lastOp = buyOperation
            strongSelf.oneProductConcurrentQueue.addOperation(buyOperation)
        }
    }
}

extension ProductsService {
    private struct Model {
        var product: Product
        var lastOp: BlockOperation?
    }

    private func saveData(success: @escaping (()->()), fail: @escaping ((Error)->())) {
        let outData = self.parser.dataFromProducts(self.products.map{$0.product})
        do {
            try self.dataManager.writeProducts(data: outData)
            DispatchQueue.main.async {
                success()
            }
        }
        catch {
            DispatchQueue.main.async {
                fail(error)
            }
        }
    }

    private func notifyUpdated(product: Product, at index: Int) {
        DispatchQueue.main.async {
            self.subscribers.forEach {$0?.productUpdated(product: product, at: index)}
        }
    }

    private func notifyAdded(product: Product) {
        DispatchQueue.main.async {
            self.subscribers.forEach {$0?.productAdded(product: product)}
        }
    }
}
