//
//  Product.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 14.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

struct Product {
    var name: String
    var quantity: Int
    var price: Double
}

extension Product : Equatable {
    static func == (leftSide: Product, rightSide: Product) -> Bool {
        var isEqual = leftSide.name == rightSide.name
        isEqual = isEqual && (leftSide.quantity == rightSide.quantity)
        isEqual = isEqual && (leftSide.price == rightSide.price)
        return isEqual
    }
}
