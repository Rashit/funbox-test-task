//
//  UIView+FitInSuperview.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 27.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

extension UIView {
    func fitInSuperview() {
        if let container = self.superview {
            if #available(iOS 11.0, *) {
                NSLayoutConstraint.activate([
                    self.topAnchor.constraint(equalTo: container.safeAreaLayoutGuide.topAnchor),
                    self.leadingAnchor.constraint(equalTo: container.safeAreaLayoutGuide.leadingAnchor),
                    self.trailingAnchor.constraint(equalTo: container.safeAreaLayoutGuide.trailingAnchor),
                    self.bottomAnchor.constraint(equalTo: container.safeAreaLayoutGuide.bottomAnchor)
                ])
            } else {
                NSLayoutConstraint.activate([
                    self.topAnchor.constraint(equalTo: container.topAnchor),
                    self.leadingAnchor.constraint(equalTo: container.leadingAnchor),
                    self.trailingAnchor.constraint(equalTo: container.trailingAnchor),
                    self.bottomAnchor.constraint(equalTo: container.bottomAnchor)
                ])
            }
        }
    }
}
