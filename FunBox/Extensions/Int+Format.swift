//
//  Int+Format.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 26.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

extension Int {
    func quantityFormat() -> String {
        return "\(self) " + Constants.quantityAbbreviation
    }
}
