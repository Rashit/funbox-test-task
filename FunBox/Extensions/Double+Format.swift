
//
//  Double+Format.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 26.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

extension Double {
    func priceFormat(currencySymbol: String, groupingSeparator: String = " ", decimalSeparator: String = ".") -> String {
        var priceString = "\(self)"
        let numberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.groupingSize = 3
        numberFormatter.groupingSeparator = groupingSeparator
        numberFormatter.decimalSeparator = decimalSeparator
        numberFormatter.minimumIntegerDigits = 1
        numberFormatter.minimumFractionDigits = (self.truncatingRemainder(dividingBy: 1) == 0) ? 0 : 2
        numberFormatter.maximumFractionDigits = 2
        if let amountString = numberFormatter.string(from: NSNumber(value: self) ){
            priceString = amountString
        }
        if currencySymbol.isEmpty {
            return priceString
        } else {
            return priceString + " " + currencySymbol
        }
    }
}
