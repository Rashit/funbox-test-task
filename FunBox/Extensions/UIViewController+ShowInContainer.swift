//
//  UIViewController+ShowInContainer.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 31.10.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

extension UIViewController {
    func showInContainer(container: UIView, in viewController: UIViewController) {
        self.view.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(self.view)
        self.view.fitInSuperview()
        viewController.addChild(self)
        self.didMove(toParent: viewController)
    }

    func removeFromContainer() {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
}
