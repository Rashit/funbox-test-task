//
//  Data+Add.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 25.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

extension Data {
    static func + (lhs: Data, rhs: Data) -> Data {
        var data = Data()
        data.append(lhs)
        data.append(rhs)

        return data
    }
}
