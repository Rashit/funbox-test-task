//
//  RootViewController.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 16.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    override func loadView() {
        let rootView = UIView()
        self.view = rootView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .black
        let loadingModule = LoadingModule().create(moduleOutput: self)
        loadingModule.presentIn(view: self.view, of: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}

extension RootViewController: LoadingModuleOutput {
    func productsDidLoaded() {
        self.displayMainStoryboard()
    }
}

extension RootViewController {
    private func displayMainStoryboard() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: type(of: self)))
        guard let viewController = storyboard.instantiateInitialViewController() else {
            return
        }
        viewController.showInContainer(container: self.view, in: self)
    }
}
