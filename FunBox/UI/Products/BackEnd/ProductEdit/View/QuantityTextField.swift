//
//  QuantityTextField.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 13.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class QuantityTextField: UITextField, Regexpable {
    var regexp: String = RegExp.integer.rawValue
}
