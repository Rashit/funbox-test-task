//
//  ProductEditViewOutput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 19.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

protocol ProductEditViewOutput {
    func viewIsReady()
    func cancelButtonTapped()
    func saveButtonTapped(editedProduct: Product?)
    func textFieldDidEndEditing(editedProduct: Product?)
}
