//
//  PriceTextField.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 13.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class PriceTextField: UITextField, Regexpable {
    var regexp: String = RegExp.money.rawValue
}
