//
//  ProductEditViewInput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 19.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

protocol ProductEditViewInput {
    func presentSelfFrom(_ viewController: UIViewController)
    func setupProduct(_ product: Product)
    func close()
    func enableSaveButton()
    func disableSaveButton()
}
