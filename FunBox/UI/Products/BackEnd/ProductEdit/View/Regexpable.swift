//
//  Regexpable.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 13.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

enum RegExp: String {
    case integer = "[0-9]*"
    case money   = "^[0-9]+(\\.)?([0-9]{1,2})?$"
}

protocol Regexpable {
    var regexp: String {get set}
    func isValid(text: String) -> Bool
}

extension Regexpable {
    func isValid(text: String) -> Bool {
        NSPredicate(format: "SELF MATCHES %@", self.regexp).evaluate(with: text)
    }
}
