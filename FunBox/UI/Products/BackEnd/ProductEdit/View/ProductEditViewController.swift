//
//  ProductEditViewController.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 12.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class ProductEditViewController: UIViewController {
    var output: ProductEditViewOutput!

    @IBOutlet private weak var nameField: UITextField!
    @IBOutlet private weak var priceField: PriceTextField!
    @IBOutlet private weak var quantityField: QuantityTextField!
    @IBOutlet private weak var scrollView: UIScrollView!

    private var activeField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.delegate = self
        self.setupTextFields()
        self.setupNavigationBar()

        self.output.viewIsReady()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.unregisterForKeyboardNotifications()
        super.viewWillDisappear(animated)
    }
}

extension ProductEditViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.contentOffset.y = 0
        }
    }
}

extension ProductEditViewController: Storyboardable {
    func storyboardName() -> String {
        return "ProductEdit"
    }
}

extension ProductEditViewController: ProductEditViewInput {
    func presentSelfFrom(_ viewController: UIViewController) {
        viewController.navigationController?.pushViewController(self, animated: true)
    }

    func close() {
        self.navigationController?.popViewController(animated: true)
    }

    func enableSaveButton() {
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }

    func disableSaveButton() {
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }

    func setupProduct(_ product: Product) {
        self.nameField.text = product.name
        self.quantityField.text = String(product.quantity)
        self.priceField.text = product.price.priceFormat(currencySymbol: "", groupingSeparator: "", decimalSeparator: Constants.decimalSeparator)
    }
}

extension ProductEditViewController {

    func unregisterForKeyboardNotifications()
    {
        NotificationCenter.default.removeObserver(self)
    }

    func registerForKeyboardNotifications()
    {
        NotificationCenter.default.addObserver(self, selector:#selector(ProductEditViewController.keyboardWasShown), name: UIResponder.keyboardDidShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector:#selector(ProductEditViewController.keyboardWillBeHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWasShown(aNotification: Notification) {
        guard let info = aNotification.userInfo,
            let activeField = self.activeField
        else {
            return
        }
        guard let kbFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        let kbSize = kbFrame.size
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
    }

    @objc func keyboardWillBeHidden() {
        let contentInsets  = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
}

extension ProductEditViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeField = nil
        self.output.textFieldDidEndEditing(editedProduct: self.editedProduct())
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let field = textField as? Regexpable else {
            return true
        }
        var isValidText = false
        if let text = textField.text,
           let textRange = Range(range, in: text)
        {
           let updatedText = text.replacingCharacters(in: textRange, with: string)
            isValidText = field.isValid(text: updatedText)
        }
        return isValidText
    }
}

extension ProductEditViewController {
    @objc private func cancelButtonTapped() {
        self.output.cancelButtonTapped()
    }

    @objc private func saveButtonTapped() {
        self.output.saveButtonTapped(editedProduct: self.editedProduct())
    }

    private func editedProduct() -> Product? {
        var editedProduct: Product? = nil
        if let name = self.nameField.text,
            let quantity = Int(self.quantityField.text ?? ""),
            let price   = Double(self.priceField.text ?? "")
        {
            editedProduct = Product(name: name, quantity: quantity, price: price)
        }
        return editedProduct
    }

    private func setupTextFields() {
        self.registerForKeyboardNotifications()
        self.nameField.delegate = self
        self.priceField.delegate = self
        self.quantityField.delegate = self

        self.nameField.setupBorderColor(UIColor.darkGray, width: 3.0)
        self.priceField.setupBorderColor(UIColor.darkGray, width: 3.0)
        self.quantityField.setupBorderColor(UIColor.darkGray, width: 3.0)
    }

    private func setupNavigationBar() {
        let cancelBtn = UIButton(type: .custom)
        cancelBtn.setTitle("Отменить", for: .normal)
        cancelBtn.applyCorporateStyle()
        cancelBtn.addTarget(self, action: #selector(ProductEditViewController.cancelButtonTapped), for: .touchUpInside)
        let cancrlItem = UIBarButtonItem(customView: cancelBtn)
        self.navigationItem.leftBarButtonItem = cancrlItem

        let saveBtn = UIButton(type: .custom)
        saveBtn.setTitle("Сохранить", for: .normal)
        saveBtn.applyCorporateStyle()
        saveBtn.addTarget(self, action: #selector(ProductEditViewController.saveButtonTapped), for: .touchUpInside)
        let saveItem = UIBarButtonItem(customView: saveBtn)
        self.navigationItem.rightBarButtonItem = saveItem

        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
}

private extension UITextField {
    func setupBorderColor(_ color: UIColor, width: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
}
