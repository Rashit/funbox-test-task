//
//  ProductEditModule.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 19.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

class ProductEditModule {
    func create(with model: ProductEditPresenter.Model, output: ProductEditModuleOutput) -> ProductEditModuleInput{
        let view = ProductEditViewController().create()
        let presenter = ProductEditPresenter(view: view, model: model, output: output)
        view.output = presenter
        return presenter
    }
}
