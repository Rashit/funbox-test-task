//
//  ProductEditModuleOutput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 19.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

protocol ProductEditModuleOutput {
    func saveProduct(_ product: Product, at index: Int)
}
