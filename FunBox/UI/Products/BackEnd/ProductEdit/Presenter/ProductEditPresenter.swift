//
//  ProductEditPresenter.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 19.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation
import UIKit

class ProductEditPresenter {
    private var view: ProductEditViewInput?
    private var moduleOutput: ProductEditModuleOutput!
    private var data: Model
    private var editedProduct: Product?

    init(view: ProductEditViewInput, model: Model, output: ProductEditModuleOutput) {
        self.view = view
        self.data = model
        self.moduleOutput = output
    }
}

extension ProductEditPresenter: ProductEditModuleInput {
    func presentFrom(_ viewController: UIViewController) {
        self.view?.presentSelfFrom(viewController)
    }
}

extension ProductEditPresenter: ProductEditViewOutput {
    func viewIsReady() {
        self.view?.setupProduct(self.data.product)
    }

    func cancelButtonTapped() {
        self.close()
    }

    func saveButtonTapped(editedProduct: Product?) {
        if let product = editedProduct {
            self.moduleOutput.saveProduct(product, at: self.data.productIndex)
        }
        self.close()
    }

    func textFieldDidEndEditing(editedProduct: Product?) {
        self.editedProduct = editedProduct
        self.refreshSaveButton()
    }
}

extension ProductEditPresenter {
    struct Model {
        var product: Product
        var productIndex: Int = NSNotFound
    }

    private func close() {
        self.view?.close()
        self.view = nil
        self.moduleOutput = nil
    }

    private func refreshSaveButton() {
        if let editedProduct = self.editedProduct {
            let isEqual = (editedProduct == self.data.product)
            if isEqual {
                self.view?.disableSaveButton()
            } else {
                self.view?.enableSaveButton()
            }
        } else {
            self.view?.disableSaveButton()
        }
    }
}
