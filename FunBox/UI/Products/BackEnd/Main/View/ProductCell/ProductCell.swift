//
//  ProductCell.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 12.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var normalView: UIView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var productQuantityLabel: UILabel!

    func setupProduct(_ product: Product, isInSave: Bool = false) {
        self.productNameLabel?.text = product.name
        self.productQuantityLabel?.text = "\(product.quantity) шт."
        if isInSave {
            self.activityIndicator?.startAnimating()
            self.contentView.bringSubviewToFront(self.loadingView)
        } else {
            self.contentView.bringSubviewToFront(self.normalView)
        }
    }
}
