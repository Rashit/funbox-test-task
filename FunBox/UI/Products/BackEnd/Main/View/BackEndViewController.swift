//
//  BackEndViewController.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 11.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class BackEndViewController: UIViewController {
    var output: BackEndViewOutput!

    lazy var loadingView: LoadingView? = {
        return LoadingView(loadingLabelText: "Loading products....")
    }()
    lazy var errorView: ErrorView? = {
        return ErrorView()
    }()

    @IBOutlet private weak var productsTableView: UITableView!

    private let cellId = "CellReuseIdentifier"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.setupTable()
        self.output.viewIsReady()
    }
}

extension BackEndViewController: BackEndViewInput {
    func presentIn(view: UIView, of viewController: UIViewController) {
        let navC = UINavigationController(rootViewController: self)
        navC.navigationBar.barTintColor = UIColor(white: 205/255, alpha: 1.0)
        navC.navigationBar.tintColor = UIColor(white: 60/255, alpha: 1.0)
        navC.showInContainer(container: view, in: viewController)
    }

    func reloadProducts() {
        self.productsTableView.reloadData()
    }
}

extension BackEndViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.output.productsCount()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = self.output.modelAt(indexPath.row)
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath) as? ProductCell {
            cell.setupProduct(model.product, isInSave: model.isInSave)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.output.didSelectProduct(at: indexPath.row)
    }
}

extension BackEndViewController {
    private func setupNavigationBar() {
        let addBtn = UIButton(type: .custom)
        addBtn.setTitle("+", for: .normal)
        addBtn.applyCorporateStyle()
        addBtn.addTarget(self, action: #selector(BackEndViewController.addProductTap), for: .touchUpInside)
        let addItem = UIBarButtonItem(customView: addBtn)

        self.navigationItem.rightBarButtonItem = addItem
    }

    private func setupTable() {
        self.productsTableView.delegate = self
        self.productsTableView.dataSource = self
        self.productsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.productsTableView.showsVerticalScrollIndicator = false
        self.productsTableView.isScrollEnabled = true
        self.productsTableView.separatorStyle = .none
        self.productsTableView.rowHeight = UITableView.automaticDimension
        self.productsTableView.estimatedRowHeight = 83

        let nib = UINib(nibName: "ProductCell", bundle: nil)
        self.productsTableView.register(nib, forCellReuseIdentifier: self.cellId)
    }

    @objc private func addProductTap() {
        self.output.addProductTapped()
    }
}
