//
//  BackEndViewInput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 19.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

protocol BackEndViewInput: Loadable {
    func presentIn(view: UIView, of viewController: UIViewController)
    func reloadProducts()
}
