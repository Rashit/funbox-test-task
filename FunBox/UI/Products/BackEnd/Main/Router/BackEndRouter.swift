//
//  BackEndRouter.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 19.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class BackEndRouter: BackEndRouterInput {
    func navigateToProductEditModule(from viewController: UIViewController, with model: ProductEditPresenter.Model, output: ProductEditModuleOutput) {
        let module = ProductEditModule().create(with: model, output: output)
        module.presentFrom(viewController)
    }
}
