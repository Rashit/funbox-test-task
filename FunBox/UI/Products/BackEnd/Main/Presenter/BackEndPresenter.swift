//
//  BackEndPresenter.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 19.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation
import UIKit

class BackEndPresenter {
    private var view: BackEndViewInput
    private var interactor: BackEndInteractorInput
    private var router: BackEndRouterInput
    private var products: [Model] = []

    init(view: BackEndViewInput, interactor: BackEndInteractorInput, router: BackEndRouterInput) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

extension BackEndPresenter: BackEndModuleInput {
    func presentIn(view: UIView, of viewController: UIViewController) {
        self.view.presentIn(view: view, of: viewController)
    }
}

extension BackEndPresenter: BackEndViewOutput {
    func viewIsReady() {
        self.view.showLoading()
        self.interactor.getProducts()
    }

    func productsCount() -> Int {
        self.products.count
    }

    func modelAt(_ index: Int) -> BackEndPresenter.Model {
        self.products[index]
    }

    func didSelectProduct(at index: Int) {
        let model = self.products[index]
        if !model.isInSave {
            self.editProduct(model.product, at: index)
        }
    }

    func addProductTapped() {
        let product = Product(name: "", quantity: 0, price: Double.zero)
        self.editProduct(product, at: NSNotFound)
    }
}

extension BackEndPresenter: ProductEditModuleOutput {
    func saveProduct(_ product: Product, at index: Int) {
        let isNewProduct = (index == NSNotFound)
        if isNewProduct {
            self.interactor.addProduct(product)
        } else {
            self.interactor.saveProduct(product, at: index)
            self.products[index].isInSave = true
            self.view.reloadProducts()
        }
    }
}

extension BackEndPresenter: BackEndInteractorOutput {
    func getProductsSuccess(products: [Product]) {
        self.view.hideLoading()
        self.products = products.map{Model(product: $0)}
        self.view.reloadProducts()
        self.interactor.subscribe(self)
    }

    func getProductsFail(error: Error) {
        let longText = """
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Turpis egestas sed tempus urna et pharetra. Dignissim cras tincidunt lobortis feugiat vivamus. Urna duis convallis convallis tellus id interdum velit laoreet. Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Pellentesque habitant morbi tristique senectus et netus. Facilisis magna etiam tempor orci. Aliquam id diam maecenas ultricies mi eget mauris pharetra. Malesuada nunc vel risus commodo. Aliquet nec ullamcorper sit amet risus nullam eget felis. Sit amet commodo nulla facilisi nullam vehicula
        """
        let errorTitle = "An error has occurred. We apologize"
        self.view.showError(title: errorTitle, message: longText, buttonTitle: "try again", action: {
            [weak self] in
            self?.view.hideError()
            self?.view.showLoading()
            self?.interactor.getProducts()
        })
    }

    func addProductSuccess(product: Product) {
        self.products.append(Model(product: product))
        self.view.reloadProducts()
    }

    func addProductFail(product: Product, error: Error) {
        print("producr NOT added")
    }

    func saveProductSuccess(product: Product, at index: Int) {
        self.products[index].isInSave = false
        self.products[index].product = product
        self.view.reloadProducts()
    }

    func saveProductFail(at index: Int, error: Error) {
        self.products[index].isInSave = false
        self.view.reloadProducts()
    }
}

extension BackEndPresenter: ProductsServiceDelegate {
    func productUpdated(product: Product, at index: Int) {
        self.products[index].product = product
        self.view.reloadProducts()
    }

    func productAdded(product: Product) {
    }
}

extension BackEndPresenter {
    struct Model {
        var product: Product
        var isInSave: Bool = false
    }

    private func editProduct(_ product: Product, at index: Int) {
           let model = ProductEditPresenter.Model(product: product, productIndex: index)
           self.router.navigateToProductEditModule(from: self.view, with: model, output: self)
    }
}
