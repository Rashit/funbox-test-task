//
//  BackEndInteractorInput.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 14.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

protocol BackEndInteractorInput {
    func getProducts()
    func subscribe(_ subscriber: ProductsServiceDelegate)
    func addProduct(_ product: Product)
    func saveProduct(_ product: Product, at index: Int)
}
