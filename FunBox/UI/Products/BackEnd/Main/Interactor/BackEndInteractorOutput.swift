//
//  BackEndInteractorOutput.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 14.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

protocol BackEndInteractorOutput: AnyObject {
    func getProductsSuccess(products: [Product])
    func getProductsFail(error: Error)
    func addProductSuccess(product: Product)
    func addProductFail(product: Product, error: Error)
    func saveProductSuccess(product: Product, at index: Int)
    func saveProductFail(at index: Int, error: Error)
}
