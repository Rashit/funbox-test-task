//
//  BackEndInteractor.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 14.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

class BackEndInteractor: BackEndInteractorInput {
    weak var output: BackEndInteractorOutput?
    private let service: ProductsServiceProtocol

    init(service: ProductsServiceProtocol) {
        self.service = service
    }

    func getProducts() {
        self.service.getProducts(success: { [weak self] model in
            self?.output?.getProductsSuccess(products: model)
        }) { [weak self] error in
            self?.output?.getProductsFail(error: error)
        }
    }

    func subscribe(_ subscriber: ProductsServiceDelegate) {
        self.service.addSubscriber(subscriber)
    }

    func addProduct(_ product: Product) {
        self.service.addProduct(product, success: { [weak self] in
            self?.output?.addProductSuccess(product: product)
        }) { [weak self] error in
            self?.output?.addProductFail(product: product, error: error)
        }
    }

    func saveProduct(_ product: Product, at index: Int) {
        self.service.saveProduct(product, at: index, success: { [weak self] in
            self?.output?.saveProductSuccess(product: product, at: index)
        }) { [weak self] error in
            self?.output?.saveProductFail(at: index, error: error)
        }
    }
}
