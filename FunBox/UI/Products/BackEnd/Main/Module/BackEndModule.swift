//
//  BackEndModule.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 19.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

class BackEndModule {
    func create() -> BackEndModuleInput{
        let view = BackEndViewController(nibName: "BackEndView", bundle: nil)
        let router = BackEndRouter()
        let service = ProductsService.shared
        let interactor = BackEndInteractor(service: service)
        let presenter = BackEndPresenter(view: view, interactor: interactor, router: router)
        view.output = presenter
        interactor.output = presenter
        return presenter
    }
}
