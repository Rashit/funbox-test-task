//
//  LoadingPresenter.swift
//  TestLoadApp
//
//  Created by Уразбахтин Рашит on 16.12.2019.
//  Copyright © 2019 Уразбахтин Рашит. All rights reserved.
//

import Foundation
import UIKit

class LoadingPresenter {
    private var view: LoadingViewInput!
    private var moduleOutput: LoadingModuleOutput!
    private var interactor: LoadingInteractorInput!

    init(view: LoadingViewInput, moduleOutput: LoadingModuleOutput, interactor: LoadingInteractorInput) {
        self.view = view
        self.moduleOutput = moduleOutput
        self.interactor = interactor
    }
}

extension LoadingPresenter: LoadingModuleInput {
    func presentIn(view: UIView, of viewController: UIViewController) {
        self.view.presentIn(view: view, of: viewController)
    }
}

extension LoadingPresenter: LoadingViewOutput {
    func viewIsReady() {
        self.view.showLoading()
        self.interactor.loadProducts()
    }

    func retryFailButtonTapped() {
        self.view.showLoading()
        self.interactor.loadProducts()
    }
}

extension LoadingPresenter: LoadingInteractorOutput {
    func loadProductsSuccess() {
        self.moduleOutput?.productsDidLoaded()
        self.view.close()
        self.view = nil
        self.moduleOutput = nil
        self.interactor = nil
    }

    func loadProductsFail(error: Error) {
        self.view.showError(title: "error title", message: "long error message", retryButtonTitle: "try again")
    }
}
