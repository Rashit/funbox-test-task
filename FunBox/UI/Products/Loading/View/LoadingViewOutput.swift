//
//  LoadingViewOutput.swift
//  TestLoadApp
//
//  Created by Уразбахтин Рашит on 16.12.2019.
//  Copyright © 2019 Уразбахтин Рашит. All rights reserved.
//

protocol LoadingViewOutput: AnyObject {
    func viewIsReady()
    func retryFailButtonTapped()
}
