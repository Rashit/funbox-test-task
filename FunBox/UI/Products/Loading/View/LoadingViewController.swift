//
//  LoadingViewController.swift
//  TestLoadApp
//
//  Created by Уразбахтин Рашит on 16.12.2019.
//  Copyright © 2019 Уразбахтин Рашит. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
    var output: LoadingViewOutput?
    private lazy var loadingView: LoadingView = {
        return LoadingView(loadingLabelText: "Загрузка...")
    }()

    private lazy var errorView: ErrorView = {
        return ErrorView()
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.output?.viewIsReady()
    }
}

extension LoadingViewController: LoadingViewInput {
    func presentIn(view: UIView, of viewController: UIViewController) {
        self.showInContainer(container: view, in: viewController)
    }

    func close() {
        self.removeFromContainer()
    }

    func showLoading() {
        if !self.view.subviews.contains(self.loadingView) {
            self.loadingView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(self.loadingView)
            self.loadingView.fitInSuperview()
        }
        self.view.bringSubviewToFront(self.loadingView)
    }

    func showError(title: String, message: String, retryButtonTitle: String) {
        if !self.view.subviews.contains(self.errorView) {
            self.errorView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(self.errorView)
            self.errorView.fitInSuperview()
        }
        self.view.bringSubviewToFront(self.errorView)
        self.errorView.show(title: title, message: message, buttonTitle: retryButtonTitle) {
            [weak self] in
            self?.output?.retryFailButtonTapped()
        }
    }
}
