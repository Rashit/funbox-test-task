//
//  LoadingViewInput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 16.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

protocol LoadingViewInput: UIViewController {
    func presentIn(view: UIView, of viewController: UIViewController)
    func showLoading()
    func showError(title: String, message: String, retryButtonTitle: String)
    func close()
}
