//
//  LoadingModuleInput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 16.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//
import UIKit

protocol LoadingModuleInput {
    func presentIn(view: UIView, of viewController: UIViewController)
}
