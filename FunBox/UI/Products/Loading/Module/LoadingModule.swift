
//
//  LoadingModule.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 16.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

class LoadingModule {
    func create(moduleOutput: LoadingModuleOutput) -> LoadingModuleInput{
        let view = LoadingViewController()
        let service = ProductsService.shared
        let interactor = LoadingInteractor(service: service)
        let presenter = LoadingPresenter(view: view, moduleOutput: moduleOutput, interactor: interactor)
        view.output = presenter
        interactor.output = presenter
        return presenter
    }
}
