//
//  LoadingModuleOutput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 16.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

protocol LoadingModuleOutput: AnyObject {
    func productsDidLoaded()
}
