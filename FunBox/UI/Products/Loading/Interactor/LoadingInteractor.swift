//
//  LoadingInteractor.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 16.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

class LoadingInteractor: LoadingInteractorInput {
    weak var output: LoadingInteractorOutput?
    private let service: ProductsServiceProtocol

    init(service: ProductsServiceProtocol) {
        self.service = service
    }

    func loadProducts() {
        self.service.initialLoadProducts(success: { [weak self] in
            self?.output?.loadProductsSuccess()
        }) { [weak self] error in
            self?.output?.loadProductsFail(error: error)
        }
    }
}
