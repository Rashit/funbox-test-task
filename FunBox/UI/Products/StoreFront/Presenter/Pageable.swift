//
//  Pageable.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 11.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

protocol Pageable {
    var pageIndex: Int { get set }
}
