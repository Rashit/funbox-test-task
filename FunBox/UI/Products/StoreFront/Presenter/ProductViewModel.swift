//
//  ProductViewModel.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 10.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

struct ProductViewModel {
    var product: Product
    var viewMode: ViewMode = .normal
}

extension ProductViewModel {
    enum ViewMode {
        case normal
        case saving
        case fail (message: String)
    }
}
