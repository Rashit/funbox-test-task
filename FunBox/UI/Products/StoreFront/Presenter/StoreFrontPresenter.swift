//
//  StoreFrontPresenter.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 18.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation
import UIKit

class StoreFrontPresenter {
    private var view: StoreFrontViewInput!
    private var interactor: StoreFrontInteractorInput!
    private var data: [ProductViewModel] = []

    init(view: StoreFrontViewInput, interactor: StoreFrontInteractorInput) {
        self.view = view
        self.interactor = interactor
    }
}

extension StoreFrontPresenter: StoreFrontModuleInput {
    func presentIn(view: UIView, of viewController: UIViewController) {
        self.view.presentIn(view: view, of: viewController)
    }
}

extension StoreFrontPresenter: StoreFrontViewOutput {
    func viewIsReady() {
        self.view.showLoading()
        self.interactor.getProducts()
    }

    func pageDataFor(index: Int) -> ProductViewModel? {
        let pageData = self.data[safe: index]
        return pageData
    }
    
    func nearIndex(for index: Int) -> Int {
        var nearIndex = self.indexAfter(include: index)
        if nearIndex == NSNotFound {
            nearIndex = self.indexBefor(exclude: index)
        }
        return nearIndex
    }

    func indexAfter(include index: Int) -> Int {
        guard self.data.indices.contains(index) else { return NSNotFound }
        var nearIndex = NSNotFound
        let rightHalf = self.data[index...]
        if let i = rightHalf.firstIndex(where: { self.isNeedToShow(page: $0) }) {
            nearIndex = i
        }
        return nearIndex
    }

    func indexBefor(exclude index: Int) -> Int {
        guard self.data.indices.contains(index) else { return NSNotFound }
        var nearIndex = NSNotFound
        let leftHalf = self.data[..<index]
        if let i = leftHalf.lastIndex(where: { self.isNeedToShow(page: $0) }) {
            nearIndex = i
        }
        return nearIndex
    }

    func buyButtonTapped(pageIndex: Int) {
        guard self.data.indices.contains(pageIndex) else { return}
        self.data[pageIndex].viewMode = .saving
        self.view.reloadCurrentPage()
        self.interactor.buyProduct(at: pageIndex)
    }

    func okBuyFailButtonTapped(pageIndex: Int) {
        guard self.data.indices.contains(pageIndex) else { return}
        self.data[pageIndex].viewMode = .normal
        self.view.reloadCurrentPage()
    }
}

extension StoreFrontPresenter: StoreFrontInteractorOutput {
    func getProductsSuccess(products: [Product]) {
        self.data = products.map{ProductViewModel(product: $0)}
        self.view.hideLoading()
        self.view.showProducts()
        self.interactor.subscribe(self)
    }

    func getProductsFail(error: Error) {
        let longText = """
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Turpis egestas sed tempus urna et pharetra. Dignissim cras tincidunt lobortis feugiat vivamus. Urna duis convallis convallis tellus id interdum velit laoreet. Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Pellentesque habitant morbi tristique senectus et netus. Facilisis magna etiam tempor orci. Aliquam id diam maecenas ultricies mi eget mauris pharetra. Malesuada nunc vel risus commodo. Aliquet nec ullamcorper sit amet risus nullam eget felis. Sit amet commodo nulla facilisi nullam vehicula
            """
        let errorTitle = "An error has occurred. We apologize"
        self.view.showError(title: errorTitle, message: longText, buttonTitle: "try again", action: {
            [weak self] in
            self?.view.hideError()
            self?.view.showLoading()
            self?.interactor.getProducts()
        })
    }

    func buyProductSuccess(at index: Int) {
        guard self.data.indices.contains(index) else { return}
        self.data[index].viewMode = .normal
        self.view.reloadCurrentPage()
    }

    func buyProductFail(at index: Int, error: Error) {
        guard self.data.indices.contains(index) else { return}
        self.data[index].viewMode = .fail(message: error.localizedDescription)
        self.view.reloadCurrentPage()
    }
}

extension StoreFrontPresenter: ProductsServiceDelegate {
    func productUpdated(product: Product, at index: Int) {
        guard self.data.indices.contains(index) else { return}
        self.data[index].product = product
        self.view.reloadCurrentPage()
    }

    func productAdded(product: Product) {
        self.data.append(ProductViewModel(product: product))
        self.view.reloadCurrentPage()
    }
}

extension StoreFrontPresenter {
    private func isNeedToShow(page: ProductViewModel) -> Bool {
        if page.product.quantity > 0 {
            return true
        } else {
            switch page.viewMode {
            case .normal:
                return false
            default:
                //for display error when try to buy product with zero quantity
                return true
            }
        }
    }
}
