//
//  StoreFrontViewOutput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 18.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

protocol StoreFrontViewOutput {
    func viewIsReady()
    func pageDataFor(index: Int) -> ProductViewModel?
    func nearIndex(for index: Int) -> Int
    func indexAfter(include index: Int) -> Int
    func indexBefor(exclude index: Int) -> Int
    func buyButtonTapped(pageIndex: Int)
    func okBuyFailButtonTapped(pageIndex: Int)
}
