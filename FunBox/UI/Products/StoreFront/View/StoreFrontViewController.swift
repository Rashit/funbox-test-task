//
//  StoreFrontViewController.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 30/10/2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation
import UIKit

class StoreFrontViewController: UIViewController {
    var output: StoreFrontViewOutput!

    // for Loadable protocol
    lazy var loadingView: LoadingView? = {
        return LoadingView(loadingLabelText: "Загрузка...")
    }()
    lazy var errorView: ErrorView? = {
        return ErrorView()
    }()

    private let pageViewContainer: UIView = {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        return view
    }()

    private lazy var pageViewController: UIPageViewController = {
        let vc = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.view.addSubview(self.pageViewContainer)
        self.pageViewContainer.fitInSuperview()
        vc.dataSource = self
        vc.showInContainer(container: self.pageViewContainer, in: self)
        return vc
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(white: 235/255, alpha: 1)
        self.output?.viewIsReady()
    }
}

extension StoreFrontViewController: StoreFrontViewInput {
    func presentIn(view: UIView, of viewController: UIViewController) {
        self.showInContainer(container: view, in: viewController)
    }

    func showProducts() {
        self.view.bringSubviewToFront(self.pageViewContainer)
        let index = self.output.nearIndex(for: 0)
        self.pageViewController.setViewControllers([self.viewControllerFor(index: index)], direction: .forward, animated: true)
    }
    
    func reloadCurrentPage() {
        var index = self.currentPageIndex()
        //to go from showing "No Products" to showing a product
        if index == NSNotFound {
            index = 0
        }
        let nearIndex = self.output.nearIndex(for: index)
        let vc = self.viewControllerFor(index: nearIndex)
        self.pageViewController.setViewControllers([vc], direction: .forward, animated: false)
    }
}

extension StoreFrontViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? Pageable else { return nil }
        let index = vc.pageIndex
        let prevIndex = self.output.indexBefor(exclude: index)
        if prevIndex == NSNotFound {
            return nil
        }
        return self.viewControllerFor(index: prevIndex)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? Pageable else { return nil }
        let index = vc.pageIndex
        let nextIindex = self.output.indexAfter(include: index + 1)
        if (nextIindex == NSNotFound) {
            return nil
        }
        return self.viewControllerFor(index: nextIindex)
    }
}


extension StoreFrontViewController: ProductInfoViewOutput {
    func okFailButtonTapped(pageIndex: Int) {
        self.output.okBuyFailButtonTapped(pageIndex: pageIndex)
    }

    func buyButtonTapped(pageIndex: Int) {
        self.output.buyButtonTapped(pageIndex: pageIndex)
    }
}

extension StoreFrontViewController {
    private func viewControllerFor(index: Int) -> UIViewController {
        let vc = ProductInfoViewController().create()

        guard let pageData = self.output.pageDataFor(index: index) else {
            vc.setupNoProductsMode()
            return vc
        }
        let product = pageData.product
        let price = product.price.priceFormat(currencySymbol: Constants.currencySymbol,
                                              groupingSeparator: Constants.groupingSeparator,
                                              decimalSeparator: Constants.decimalSeparator)
        let productModel = ProductInfoViewController.Model(name: product.name, price: price, quantity: product.quantity.quantityFormat())
        vc.output = self
        vc.pageIndex = index
        vc.model = productModel
        switch pageData.viewMode {
        case .normal:
            vc.setupNormalMode()
        case .saving:
            vc.setupSavingMode()
        case .fail(let message):
            vc.setupFailMode(message: message)
        }
        return vc
    }

    private func currentPageIndex() -> Int{
        var index: Int = NSNotFound
        if let currentViewController = self.pageViewController.viewControllers?.first,
            let pageble = currentViewController as? Pageable {
            index = pageble.pageIndex
        }
        return index
    }
}
