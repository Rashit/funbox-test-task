//
//  StoreFrontViewInput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 18.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

protocol StoreFrontViewInput: Loadable {
    func presentIn(view: UIView, of viewController: UIViewController)
    func reloadCurrentPage()
    func showProducts()
}
