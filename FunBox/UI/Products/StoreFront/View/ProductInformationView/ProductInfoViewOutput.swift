//
//  ProductInfoViewOutput.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 21.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

protocol ProductInfoViewOutput {
    func buyButtonTapped(pageIndex: Int)
    func okFailButtonTapped(pageIndex: Int)
}
