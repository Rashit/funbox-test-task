//
//  ProductInfoViewController.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 06.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit


class ProductInfoViewController: UIViewController, Pageable {
    var output: ProductInfoViewOutput!
    var pageIndex: Int = NSNotFound
    var model: Model?

    @IBOutlet private weak var failView: UIView!
    @IBOutlet private weak var errorViewContainer: UIView!
    @IBOutlet private weak var noProductsView: UIView!
    @IBOutlet private weak var savingView: UIView!
    @IBOutlet private weak var normalView: UIView!
    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var productPriceLabel: UILabel!
    @IBOutlet private weak var productQuantityLabel: UILabel!
    @IBOutlet private weak var buyButton: UIButton!

    private lazy var errorView: ErrorView = {
        let view = ErrorView(alpha: 0.75)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.errorViewContainer.addSubview(view)
        view.fitInSuperview()
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.buyButton.applyCorporateStyle()

        if let model = self.model {
            self.setupProduct(with: model)
        }
    }

    func setupSavingMode() {
        self.view.bringSubviewToFront(self.savingView)
    }

    func setupNormalMode() {
        self.view.bringSubviewToFront(self.normalView)
    }

    func setupNoProductsMode() {
        self.view.bringSubviewToFront(self.noProductsView)
    }

    func setupFailMode(message: String) {
        self.view.bringSubviewToFront(self.failView)
        self.errorView.show(title: "we regret", message: message, buttonTitle: "ok") {
            [weak self] in
            self?.okFailButtonTap()
        }
    }
}

extension ProductInfoViewController: Storyboardable {
    func storyboardName() -> String {
        return "ProductInfo"
    }
}

extension ProductInfoViewController {
    struct Model {
        var name: String
        var price: String
        var quantity: String
    }

    private func setupProduct(with model: Model) {
        self.productNameLabel?.text     = model.name
        self.productPriceLabel?.text    = model.price
        self.productQuantityLabel?.text = model.quantity
    }

    @IBAction private func buyButtonTap(_ sender: Any) {
        self.output?.buyButtonTapped(pageIndex: self.pageIndex)
    }

    private func okFailButtonTap() {
        self.output?.okFailButtonTapped(pageIndex: self.pageIndex)
    }
}
