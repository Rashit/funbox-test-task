//
//  StoreFrontModule.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 18.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

class StoreFrontModule {
    func create() -> StoreFrontModuleInput{
        let view = StoreFrontViewController()
        let service = ProductsService.shared
        let interactor = StoreFrontInteractor(service: service)
        let presenter = StoreFrontPresenter(view: view, interactor: interactor)
        view.output = presenter
        interactor.output = presenter
        return presenter
    }
}
