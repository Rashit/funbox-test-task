//
//  StoreFrontModuleInput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 18.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

protocol StoreFrontModuleInput {
    func presentIn(view: UIView, of viewController: UIViewController)
}
