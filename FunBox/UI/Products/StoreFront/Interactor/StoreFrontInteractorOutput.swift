//
//  StoreFrontInteractorOutput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 18.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

protocol StoreFrontInteractorOutput: AnyObject {
    func getProductsSuccess(products: [Product])
    func getProductsFail(error: Error)
    func buyProductSuccess(at index: Int)
    func buyProductFail(at index: Int, error: Error)
}
