//
//  StoreFrontInteractor.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 18.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

class StoreFrontInteractor: StoreFrontInteractorInput {
    weak var output: StoreFrontInteractorOutput?
    private let service: ProductsServiceProtocol

    init(service: ProductsServiceProtocol) {
        self.service = service
    }

    func getProducts() {
        self.service.getProducts(success: { [weak self] model in
            self?.output?.getProductsSuccess(products: model)
        }) { [weak self] error in
            self?.output?.getProductsFail(error: error)
        }
    }

    func subscribe(_ subscriber: ProductsServiceDelegate) {
        self.service.addSubscriber(subscriber)
    }

    func buyProduct(at index: Int) {
        self.service.buyProduct(at: index, success: { [weak self] in
            self?.output?.buyProductSuccess(at: index)
        }) { [weak self] error in
            self?.output?.buyProductFail(at: index, error: error)
        }
    }
}
