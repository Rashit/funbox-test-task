//
//  StoreFrontInteractorInput.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 18.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

protocol StoreFrontInteractorInput {
    func getProducts()
    func subscribe(_ subscriber: ProductsServiceDelegate)
    func buyProduct(at index: Int)
}
