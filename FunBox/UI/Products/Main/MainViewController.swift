//
//  MainViewController.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 30/10/2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet private weak var storeFrontContainer: UIView!
    @IBOutlet private weak var backEndContainer: UIView!
    @IBOutlet private weak var segmentedControlContainer: UIView!

    private var segmentedControl: TwoSegmentControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        let storeFrontModule = StoreFrontModule().create()
        storeFrontModule.presentIn(view: self.storeFrontContainer, of: self)

        let backEndModule = BackEndModule().create()
        backEndModule.presentIn(view: self.backEndContainer, of: self)


        self.segmentedControl = TwoSegmentControl(leadingSegmentTitle: "Store-Front",
                                                  trailingSegmentTitle: "Back-End",
                                                  selectedColor: Constants.selectedSegmentColor,
                                                  action: { [weak self] (sender) in
                                                    self?.segmentChange(sender)
        })
        self.segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        self.segmentedControlContainer.addSubview(self.segmentedControl)
        self.segmentedControl.fitInSuperview()

        self.view.bringSubviewToFront(self.storeFrontContainer)
    }
}

extension MainViewController {    
    private func segmentChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.view.bringSubviewToFront(self.storeFrontContainer)
        } else {
            self.view.bringSubviewToFront(self.backEndContainer)
        }
    }
}
