//
//  LoadingView.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 29.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    private let activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView()
        ai.color = .gray
        ai.translatesAutoresizingMaskIntoConstraints = false
        ai.startAnimating()
        return ai
    }()

    private let label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = "Loading..."
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    init(loadingLabelText: String) {
        super.init(frame: CGRect.zero)

        self.addSubview(self.activityIndicator)
        NSLayoutConstraint.activate([
            self.activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])

        self.addSubview(self.label)
        NSLayoutConstraint.activate([
            self.label.topAnchor.constraint(equalTo: self.activityIndicator.bottomAnchor, constant: 15),
            self.label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
            self.label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15)
        ])

        self.label.text = loadingLabelText
        self.backgroundColor = .white
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
