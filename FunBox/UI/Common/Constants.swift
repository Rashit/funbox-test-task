//
//  Constants.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 21.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//
import Foundation
import UIKit

struct Constants {
    static let productBuyDurationSeconds: UInt32 = 15
    static let productSaveDurationSeconds: UInt32 = 25
    static let productsLoadDurationSeconds: UInt32 = 3//5
    static let quantityAbbreviation  = "шт." // pcs.
    static let currencySymbol        = "р" //Locale.current.currencySymbol
    static let groupingSeparator     = " " //Locale.current.groupingSeparator
    static let decimalSeparator      = "." //Locale.current.decimalSeparator
    static let selectedSegmentColor  = UIColor(red: 165/255, green: 222/255, blue: 250/255, alpha: CGFloat(1))
    static let buttonBackgroundColor = UIColor(white: 235/255, alpha: 1.0).cgColor
}
