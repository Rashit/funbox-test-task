//
//  Loadable.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 29.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

protocol Loadable: UIViewController {
    var loadingView: LoadingView? { get }
    var errorView:   ErrorView? { get }
    func showLoading()
    func hideLoading()
    func showError(title: String, message: String, buttonTitle: String?, action: (()->())?)
    func hideError()
}

extension Loadable { //where Self: UIViewController {
    func showLoading() {
        guard let loadingView = self.loadingView else { return }

        if !self.view.subviews.contains(loadingView) {
            loadingView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(loadingView)
            loadingView.fitInSuperview()
        }
        self.view.bringSubviewToFront(loadingView)
    }

    func hideLoading() {
        self.loadingView?.removeFromSuperview()
    }

    func showError(title: String, message: String, buttonTitle: String? = nil, action: (()->())?) {
        guard let errorView = self.errorView else { return }

        if !self.view.subviews.contains(errorView) {
            errorView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(errorView)
            errorView.fitInSuperview()
        }
        self.view.bringSubviewToFront(errorView)
        errorView.show(title: title, message: message, buttonTitle: buttonTitle, action: action)
    }

    func hideError() {
        self.errorView?.removeFromSuperview()
    }
}
