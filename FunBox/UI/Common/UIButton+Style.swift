//
//  UIButton+Style.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 03.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

extension UIButton {
    func applyCorporateStyle() {
        self.layer.cornerRadius = CGFloat(8)
        self.layer.borderWidth  = CGFloat(2)
        self.layer.borderColor  = UIColor.darkGray.cgColor
        self.layer.backgroundColor = Constants.buttonBackgroundColor
        self.setTitleColor(.darkGray, for: .normal)
        self.setTitleColor(.lightGray, for: .disabled)
        self.contentEdgeInsets = UIEdgeInsets.init(top: 5, left: 15, bottom: 5, right: 15)
    }
}
