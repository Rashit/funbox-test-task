//
//  TwoSegmentControl.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 27.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class TwoSegmentControl: UIView {
    private var action: ((_ sender: UISegmentedControl) -> ())?

    private var segmentedControl: UISegmentedControl = {
        let sc = UISegmentedControl()
        sc.translatesAutoresizingMaskIntoConstraints = false
        sc.backgroundColor = .clear
        sc.tintColor = .clear
        if #available(iOS 13.0, *) {
            sc.selectedSegmentTintColor = .clear
        }
        let font = UIFont.preferredFont(forTextStyle: .body)
        sc.setTitleTextAttributes([NSAttributedString.Key.font: font,
                                   NSAttributedString.Key.foregroundColor: UIColor.darkGray],
                                  for: .normal)
        sc.setTitleTextAttributes([NSAttributedString.Key.font: font,
                                   NSAttributedString.Key.foregroundColor: UIColor.darkText],
                                  for: .selected)
        return sc
    }()

    private var selectedSegmentBackground: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.orange
        view.layer.borderColor = UIColor.darkGray.cgColor
        view.layer.borderWidth = CGFloat(2)
        return view
    }()

    private lazy var leadingConstraints: [NSLayoutConstraint] = {
        return [
            self.selectedSegmentBackground.topAnchor.constraint(equalTo: self.segmentedControl.topAnchor),
            self.selectedSegmentBackground.bottomAnchor.constraint(equalTo: self.segmentedControl.bottomAnchor),
            self.selectedSegmentBackground.leadingAnchor.constraint(equalTo: self.segmentedControl.leadingAnchor),
            self.selectedSegmentBackground.widthAnchor.constraint(equalTo: self.segmentedControl.widthAnchor, multiplier: 1/2)
        ]
    }()

    private lazy var trailingConstraints: [NSLayoutConstraint] = {
        return [
            self.selectedSegmentBackground.topAnchor.constraint(equalTo: self.segmentedControl.topAnchor),
            self.selectedSegmentBackground.bottomAnchor.constraint(equalTo: self.segmentedControl.bottomAnchor),
            self.selectedSegmentBackground.trailingAnchor.constraint(equalTo: self.segmentedControl.trailingAnchor),
            self.selectedSegmentBackground.widthAnchor.constraint(equalTo: self.segmentedControl.widthAnchor, multiplier: 1/2)
        ]
    }()

    init(leadingSegmentTitle: String, trailingSegmentTitle: String, selectedColor: UIColor, action: ((_ sender: UISegmentedControl)->())?) {
        super.init(frame: CGRect.zero)
        
        self.action = action
        self.segmentedControl.insertSegment(withTitle: leadingSegmentTitle, at: 0, animated: true)
        self.segmentedControl.insertSegment(withTitle: trailingSegmentTitle, at: 1, animated: true)
        self.segmentedControl.selectedSegmentIndex = 0
        self.segmentedControl.addTarget(self, action: #selector(TwoSegmentControl.segmentChange(_:)), for: .valueChanged)

        self.addSubview(self.selectedSegmentBackground)
        self.addSubview(self.segmentedControl)
        self.segmentedControl.fitInSuperview()

        NSLayoutConstraint.activate(self.leadingConstraints)

        self.backgroundColor = UIColor(white: 205/255, alpha: 1.0)
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.borderWidth = CGFloat(2)

        self.selectedSegmentBackground.backgroundColor = selectedColor
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension TwoSegmentControl {
    @objc private func segmentChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            NSLayoutConstraint.deactivate(self.trailingConstraints)
            NSLayoutConstraint.activate(self.leadingConstraints)
            UIView.animate(withDuration: 0.3) {
                self.layoutIfNeeded()
            }
        } else {
            NSLayoutConstraint.deactivate(self.leadingConstraints)
            NSLayoutConstraint.activate(self.trailingConstraints)
            UIView.animate(withDuration: 0.3) {
                self.layoutIfNeeded()
            }
        }
        self.action?(sender)
    }
}
