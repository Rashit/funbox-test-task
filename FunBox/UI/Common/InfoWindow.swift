//
//  InfoWindow.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 03.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class InfoWindow: UIView {

    private let info: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentHuggingPriority(UILayoutPriority(251), for: .vertical)
        label.setContentHuggingPriority(UILayoutPriority(251), for: .horizontal)
        label.backgroundColor = .clear
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = UIColor.darkText
        label.numberOfLines = 0
        label.textAlignment = .left
        label.text = ""
        return label
    }()

    init() {
        super.init(frame: CGRect.zero)

        self.layer.cornerRadius = CGFloat(8)
        self.layer.borderWidth  = CGFloat(1)
        self.layer.borderColor  = UIColor.darkGray.cgColor
        self.backgroundColor = .white

        self.addInfo()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupInfo(text: String) {
        self.info.text = text
    }
}

extension InfoWindow {
    private func addInfo() {
        self.addSubview(self.info)
        NSLayoutConstraint.activate([
            self.info.topAnchor.constraint(equalTo: self.topAnchor, constant: 15),
            self.info.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
            self.info.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),
            self.info.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -15)
        ])
    }
}
