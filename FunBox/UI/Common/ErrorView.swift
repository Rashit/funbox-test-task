//
//  ErrorView.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 02.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

class ErrorView: UIView {
    private let translucentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .clear
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private let container: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentHuggingPriority(UILayoutPriority(251), for: .vertical)
        label.setContentHuggingPriority(UILayoutPriority(251), for: .horizontal)
        label.backgroundColor = .clear
        label.font = UIFont.preferredFont(forTextStyle: .title2)
        label.textColor = UIColor.gray
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = ""
        return label
    }()

    private let message: InfoWindow = {
        let view = InfoWindow()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let button: UIButton = {
        let button = UIButton()
        button.setContentHuggingPriority(UILayoutPriority(251), for: .vertical)
        button.applyCorporateStyle()
        button.isHidden = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(okTapped), for: .touchUpInside)
        return button
    }()


    private var action: (()->())? = nil

    init(alpha: CGFloat = 1) {
        super.init(frame: CGRect.zero)

        self.translucentView.backgroundColor = UIColor(white: 235/255, alpha: alpha)
        self.addTranslucentView()
        self.addScrollView()
        self.addContainer()
        self.addTitle()
        self.addMessage()
        self.addOkButton()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func show(title: String, message: String, buttonTitle: String? = nil, action: (()->())?) {
        self.title.text = title
        self.message.setupInfo(text: message)
        if let buttonTitle = buttonTitle {
            self.button.isHidden = false
            self.button.setTitle(buttonTitle, for: .normal)
        } else {
            self.button.isHidden = true
        }
        self.action = action
    }
}

extension ErrorView {
    private func addTranslucentView() {
        self.addSubview(self.translucentView)
        self.translucentView.fitInSuperview()
    }

    private func addScrollView() {
        self.addSubview(self.scrollView)
        NSLayoutConstraint.activate([
            self.scrollView.topAnchor.constraint(equalTo: self.topAnchor),
            self.scrollView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.scrollView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }

    private func addContainer() {
        self.scrollView.addSubview(self.container)
        let height = self.container.heightAnchor.constraint(equalTo: self.scrollView.heightAnchor)
        height.priority = UILayoutPriority(249)
        NSLayoutConstraint.activate([
            self.container.topAnchor.constraint(equalTo: self.scrollView.topAnchor),
            self.container.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor),
            self.container.trailingAnchor.constraint(equalTo: self.scrollView.trailingAnchor),
            self.container.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor),
            //to prevent any horizontal scrolling
            self.container.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor),
            height
        ])
    }

    private func addTitle() {
        self.container.addSubview(self.title)
        NSLayoutConstraint.activate([
            self.title.topAnchor.constraint(equalTo: self.container.topAnchor, constant: 50),
            self.title.leadingAnchor.constraint(equalTo: self.container.leadingAnchor, constant: 15),
            self.title.trailingAnchor.constraint(equalTo: self.container.trailingAnchor, constant: -15)
        ])
    }

    private func addMessage() {
        self.container.addSubview(self.message)
        NSLayoutConstraint.activate([
            self.message.topAnchor.constraint(equalTo: self.title.bottomAnchor, constant: 25),
            self.message.leadingAnchor.constraint(equalTo: self.container.leadingAnchor, constant: 15),
            self.message.trailingAnchor.constraint(equalTo: self.container.trailingAnchor, constant: -15)
        ])
    }

    private func addOkButton() {
        self.container.addSubview(self.button)
        NSLayoutConstraint.activate([
            self.button.topAnchor.constraint(equalTo: self.message.bottomAnchor, constant: 20),
            self.button.centerXAnchor.constraint(equalTo: self.container.centerXAnchor),
            self.button.bottomAnchor.constraint(equalTo: self.container.bottomAnchor, constant: -50)
        ])
    }

    @objc private func okTapped() {
        self.action?()
    }
}
