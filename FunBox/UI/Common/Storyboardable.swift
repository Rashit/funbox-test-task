//
//  Storyboardable.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 11.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

protocol Storyboardable where Self: UIViewController {
    func storyboardName() -> String
}

extension Storyboardable {
    func create() -> Self {
        let sbName = self.storyboardName()
        let storyboard = UIStoryboard(name: sbName, bundle: nil)
        let className = NSStringFromClass(Self.self)
        let finalClassName = className.components(separatedBy: ".").last!
        let viewControllerId = finalClassName
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerId)

        return viewController as! Self
    }
}
