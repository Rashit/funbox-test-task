//
//  ProductsDataManagerProtocol.swift
//  FunBox
//
//  Created by Уразбахтин Рашит on 09.12.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

protocol ProductsDataManagerProtocol {
    func readProducts() throws -> Data
    func writeProducts(data: Data) throws
}
