//
//  ProductsFileManager.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 18.11.2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import Foundation

class ProductsFileManager {

    private let fileName: String

    private lazy var productsFileURL: URL? = {
        let dir: URL? = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = dir?.appendingPathComponent(self.fileName)
        return fileURL
    }()

    init(fileName: String) {
        self.fileName = fileName
    }

}

extension ProductsFileManager: ProductsDataManagerProtocol {
    func readProducts() throws -> Data {
        var data = Data()
        if let fileURL = self.productsFileURL {
            data = try Data(contentsOf: fileURL)
        }
        return data
    }

    func writeProducts(data: Data) throws {
        if let fileURL = self.productsFileURL {
            try data.write(to: fileURL)
        }
    }
}
