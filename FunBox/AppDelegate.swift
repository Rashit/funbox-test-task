//
//  AppDelegate.swift
//  FunBox
//
//  Created by Rashit Urazbakhtin on 30/10/2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let productsFileName = "products.csv"

        // after switch between data formats need remove app from device or simulator
        let dataFormat: DataFormat = .bin
        try? self.makeWritableCopy(named: productsFileName, ofResourceFile: dataFormat.fileName)
        ProductsService.setup(ProductsService.Config(dataManager: ProductsFileManager(fileName: productsFileName), parser: dataFormat.parser))

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = RootViewController()
        self.window?.makeKeyAndVisible()
        
        return true
    }
}

extension AppDelegate {
    private enum DataFormat {
        case csv
        case bin

        var fileName: String {
            switch self {
            case .csv:
                return "data.csv"
            case .bin:
                return "data.bin"
            }
        }

        var parser: ProductParserProtocol {
            switch self {
            case .csv:
                return ProductCSVParser()
            case .bin:
                return ProductBinParser()
            }
        }
    }

    private func makeWritableCopy(named destFileName: String, ofResourceFile originalFileName: String) throws {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else {
            fatalError("No document directory found in application bundle.")
        }
        let writableFileURL = documentsDirectory.appendingPathComponent(destFileName)
        if (try? writableFileURL.checkResourceIsReachable()) == nil {
            guard let originalFileURL = Bundle.main.url(forResource: originalFileName, withExtension: nil) else {
                fatalError("Cannot find original file “\(originalFileName)” in application bundle’s resources.")
            }
            let originalContents = try Data(contentsOf: originalFileURL)
            try originalContents.write(to: writableFileURL, options: .atomic)
        }
    }
}
