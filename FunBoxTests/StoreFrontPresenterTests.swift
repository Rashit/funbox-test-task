//
//  StoreFrontPresenterTests.swift
//  StoreFrontPresenterTests
//
//  Created by Rashit Urazbakhtin on 30/10/2019.
//  Copyright © 2019 Rashit Urazbakhtin. All rights reserved.
//

import XCTest
@testable import FunBox

class StoreFrontPresenterTests: XCTestCase {

    class MockView: UIViewController, StoreFrontViewInput {
        var loadingView: LoadingView?

        var errorView: ErrorView?

        var presentInIsCalled = false
        func presentIn(view: UIView, of viewController: UIViewController) {
            self.presentInIsCalled = true
        }

        var reloadCurrentPageIsCalled = false
        func reloadCurrentPage() {
            self.reloadCurrentPageIsCalled = true
        }

        var showProductsIsCalled = false
        func showProducts() {
            self.showProductsIsCalled = true
        }

        var showLoadingIsCalled = false
        func showLoading() {
            self.showLoadingIsCalled = true
        }

        var hideLoadingIsCalled = false
        func hideLoading() {
            self.hideLoadingIsCalled = true
        }

        var showErrorIsCalled = false
        var showErrorAction: (() -> ())?
        func showError(title: String, message: String, buttonTitle: String? = nil, action: (() -> ())?) {
            self.showErrorIsCalled = true
            self.showErrorAction = action
        }

        var hideErrorIsCalled = false
        func hideError() {
            self.hideErrorIsCalled = true
        }
    }

    class MockInteractor: StoreFrontInteractorInput {
        var getProductsIsCalled = false
        func getProducts() {
            self.getProductsIsCalled = true
        }

        var subscribeIsCalled = false
        func subscribe(_ subscriber: ProductsServiceDelegate) {
            self.subscribeIsCalled = true
        }

        var buyProductIsCalled = false
        func buyProduct(at index: Int) {
            self.buyProductIsCalled = true
        }
    }

    var mockView: MockView!
    var mockInteractor: MockInteractor!
    var presenter: StoreFrontPresenter!
    override func setUp() {
        self.mockView = MockView()
        self.mockInteractor = MockInteractor()
        self.presenter = StoreFrontPresenter(view: self.mockView, interactor: self.mockInteractor)
    }

    func testPresentIn() {
        self.presenter.presentIn(view: UIView(), of: UIViewController())

        XCTAssertTrue(self.mockView.presentInIsCalled)
    }

    func testViewIsReady() {
        self.presenter.viewIsReady()

        XCTAssertTrue(self.mockView.showLoadingIsCalled)
        XCTAssertTrue(self.mockInteractor.getProductsIsCalled)
    }

    func testPageDataFor() {
        //prepare
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 2, price: 2.0)
        let product3 = Product(name: "p3", quantity: 3, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        //test code
        let pageData1 = self.presenter.pageDataFor(index: 0)
        let pageData2 = self.presenter.pageDataFor(index: 1)
        let pageData3 = self.presenter.pageDataFor(index: 2)

        //result
        XCTAssertTrue(pageData1?.product == product1)
        XCTAssertTrue(pageData2?.product == product2)
        XCTAssertTrue(pageData3?.product == product3)
    }

    func testNearIndex() {
        //prepare
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 2, price: 2.0)
        let product3 = Product(name: "p3", quantity: 0, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        //test code
        let nearIndex0 = self.presenter.nearIndex(for: 0)
        let nearIndex2 = self.presenter.nearIndex(for: 2)

        //result
        XCTAssertTrue(nearIndex0 == 0)
        XCTAssertTrue(nearIndex2 == 1)
    }

    func testIndexAfter() {
        //prepare
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 0, price: 2.0)
        let product3 = Product(name: "p3", quantity: 0, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        //test code
        let indexAfter0 = self.presenter.indexAfter(include: 0)
        let indexAfter1 = self.presenter.indexAfter(include: 1)
        let indexAfter5 = self.presenter.indexAfter(include: 5)

        //result
        XCTAssertTrue(indexAfter0 == 0)
        XCTAssertTrue(indexAfter1 == NSNotFound)
        XCTAssertTrue(indexAfter5 == NSNotFound)
    }

    func testIndexBeforInNormalMode() {
        //prepare
        let product1 = Product(name: "p1", quantity: 0, price: 1.0)
        let product2 = Product(name: "p2", quantity: 1, price: 2.0)
        let product3 = Product(name: "p3", quantity: 1, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        //test code
        let indexBefor0 = self.presenter.indexBefor(exclude: 0)
        let indexBefor2 = self.presenter.indexBefor(exclude: 2)
        let indexBefor5 = self.presenter.indexBefor(exclude: 5)

        //reslt
        XCTAssertTrue(indexBefor0 == NSNotFound)
        XCTAssertTrue(indexBefor2 == 1)
        XCTAssertTrue(indexBefor5 == NSNotFound)
    }

    func testIndexInNotNormalMode() {
        //prepare
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 0, price: 2.0)
        let product3 = Product(name: "p3", quantity: 1, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)
        self.presenter.buyProductFail(at: 1, error: ProductError.insufficientQuantityProduct)

        //test code
        let indexAfter1 = self.presenter.indexAfter(include: 1)
        let indexBefor2 = self.presenter.indexBefor(exclude: 2)

        //result
        XCTAssertTrue(indexAfter1 == 1)
        XCTAssertTrue(indexBefor2 == 1)
    }

    func testBuyButtonTap() {
        //prepare
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 0, price: 2.0)
        let product3 = Product(name: "p3", quantity: 1, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        //test code
        self.presenter.buyButtonTapped(pageIndex: 0)

        //result
        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled)
        XCTAssertTrue(self.mockInteractor.buyProductIsCalled)
    }

    func testBuyButtonTapOutOfRange() {
        self.presenter.buyButtonTapped(pageIndex: 100)

        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled == false)
        XCTAssertTrue(self.mockInteractor.buyProductIsCalled == false)
    }

    func testOkBuyFailButtonTap() {
        //prepare
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 0, price: 2.0)
        let product3 = Product(name: "p3", quantity: 1, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        //test code
        self.presenter.okBuyFailButtonTapped(pageIndex: 1)

        //result
        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled)
    }

    func testOkBuyFailButtonTapOutOfRange() {
        self.presenter.okBuyFailButtonTapped(pageIndex: 100)

        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled == false)
    }

    func testGetProductsSuccess() {
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 0, price: 2.0)
        let product3 = Product(name: "p3", quantity: 1, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        XCTAssertTrue(self.mockView.hideLoadingIsCalled)
        XCTAssertTrue(self.mockView.showProductsIsCalled)
        XCTAssertTrue(self.mockInteractor.subscribeIsCalled)
    }

    func testGetProductsFail() {
        self.presenter.getProductsFail(error: ProductError.insufficientQuantityProduct)

        XCTAssertTrue(self.mockView.showErrorIsCalled)
    }

    func testGetProductsFailAction() {
        //prepare
        self.presenter.getProductsFail(error: ProductError.insufficientQuantityProduct)

        //test code
        self.mockView.showErrorAction?()

        //result
        XCTAssertTrue(self.mockView.hideErrorIsCalled)
        XCTAssertTrue(self.mockView.showLoadingIsCalled)
        XCTAssertTrue(self.mockInteractor.getProductsIsCalled)
    }

    func testBuyProductSuccess() {
        //prepare
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 0, price: 2.0)
        let product3 = Product(name: "p3", quantity: 1, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        //test code
        self.presenter.buyProductSuccess(at: 0)

        //result
        let pageData = self.presenter.pageDataFor(index: 0)
        switch pageData?.viewMode {
        case .normal:
            XCTAssertTrue(true)
        default:
            XCTFail()
        }
        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled)
    }

    func testBuyProductSuccessOutOfRange() {
        self.presenter.buyProductSuccess(at: 0)

        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled == false)
    }

    func testBuyProductFail() {
        //prepare
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 0, price: 2.0)
        let product3 = Product(name: "p3", quantity: 1, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        //test code
        self.presenter.buyProductFail(at: 0, error: ProductError.insufficientQuantityProduct)

        //result
        let pageData = self.presenter.pageDataFor(index: 0)
        switch pageData?.viewMode {
        case .fail(let message):
            XCTAssertTrue(message == ProductError.insufficientQuantityProduct.localizedDescription)
        default:
            XCTFail()
        }
        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled)
    }

    func testBuyProductFailOutOfRange() {
        self.presenter.buyProductFail(at: 0, error: ProductError.insufficientQuantityProduct)

        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled == false)
    }

    func testProductUpdated() {
        //prepare
        let product1 = Product(name: "p1", quantity: 1, price: 1.0)
        let product2 = Product(name: "p2", quantity: 0, price: 2.0)
        let product3 = Product(name: "p3", quantity: 1, price: 3.0)
        let products = [product1, product2, product3]
        self.presenter.getProductsSuccess(products: products)

        //test code
        let updatedProduct1 = Product(name: "p1_updated", quantity: 5, price: 1.5)
        self.presenter.productUpdated(product: updatedProduct1, at: 0)

        //result
        let pageData = self.presenter.pageDataFor(index: 0)
        XCTAssertTrue(pageData?.product == updatedProduct1)
        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled)
    }

    func testProductUpdatedOutOfRange() {
        let updatedProduct1 = Product(name: "p1_updated", quantity: 5, price: 1.5)
        self.presenter.productUpdated(product: updatedProduct1, at: 0)

        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled == false)
    }

    func testProductAdded() {
        //prepare
        self.presenter.getProductsSuccess(products: [])

        //test code
        let newProduct = Product(name: "new_product", quantity: 15, price: 21.5)
        self.presenter.productAdded(product: newProduct)

        //result
        let pageData = self.presenter.pageDataFor(index: 0)
        XCTAssertTrue(pageData?.product == newProduct)
        XCTAssertTrue(self.mockView.reloadCurrentPageIsCalled)
    }
}
